# Git Flow

The overall flow of Gitflow is:

1. A develop branch is created from main
2. A release branch is created from develop
3. Feature branches are created from develop
4. When a feature is complete it is merged into the develop branch
5. When the release branch is done it is merged into develop and main
6. If an issue in main is detected a hotfix branch is created from main
7. Once the hotfix is complete it is merged to both develop and main

## Develop and Main branches

- The main branch stores the official release history.
- The develop branch serves as an integration branch for features.

```bash
git branch develop
git push -u origin develop
```

## Feature branches

- The feature branches use develop as their parent branch
- When a feature is complete, it gets merged back into develop.

```bash
# start new feature branch branch
git checkout develop
git checkout -b feature/setup-commitlint-husky

# finish feature branch, merged into develop branch
git checkout develop
git merge --no-ff feature/setup-commitlint-husky

# push changes to the server
git push origin develop
git push origin feature/setup-commitlint-husky
```

## Release branches

- Creating this branch starts the next release cycle, so no new features can be added after this point—only bug fixes, documentation generation, and other release-oriented tasks should go in this branch.
- The release branch gets merged into main and tagged with a version number.
- It should be merged back into develop, which may have progressed since the release was initiated.

```bash
# start new release branch from develop branch
git checkout develop
git checkout -b release/v0.1.0

# finish release branch, merged into main branch
git checkout main
git merge --no-ff release/v0.1.0

# push changes to the server
git push origin main
git push origin release/v0.1.0
```

# Hotfix branches

- Hotfix branches are used to quickly patch production releases.

```bash
# start new hotfix branch from main branch
git checkout main
git checkout -b hotfix/fix-invalid-path

# finish hotfix branch, merged into main and develop branches
git checkout main
git merge --no-ff hotfix/fix-invalid-path
git checkout develop
git merge --no-ff hotfix/fix-invalid-path

# push changes to the server
git push origin main
git push origin develop
git push origin hotfix/fix-invalid-path
```

# References

https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow?fbclid=IwAR13CNzL8u9ZwnAWjKezSmI1MPWVsv-yAJWEvsJ-HukCYZyX-VTd9mnrga0

https://stackoverflow.com/questions/4470523/create-a-branch-in-git-from-another-branch
